package org.mascarene.matrix.client.r0.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {
    private String errcode;
    private String error;

    public ApiError(String errcode) {
        this.errcode = errcode;
    }
}
