package org.mascarene.matrix.client.r0.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public final class LoginAuthFlow {
    private List<LoginFlow> flows;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class LoginFlow {
        @JsonProperty("type")
        private String flowType;
    }
}
