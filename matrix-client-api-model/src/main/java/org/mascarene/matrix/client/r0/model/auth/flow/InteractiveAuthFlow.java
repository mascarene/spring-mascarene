package org.mascarene.matrix.client.r0.model.auth.flow;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InteractiveAuthFlow {
    private @With List<String> completed;
    private @With String errcode;
    private @With String error;
    private List<Flow> flows;
    private Map<String, Map<String, String>> params;
    private @With String session;
}
