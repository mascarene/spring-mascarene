package org.mascarene.matrix.client.r0.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WellKnownResponse {
    @JsonProperty("m.homeserver")
    private HomeServerInformation mHomeServer;
    @JsonProperty("m.identity_server")
    private HomeServerInformation mIdentityServer;

    public WellKnownResponse(HomeServerInformation homeServerInformation) {
        this.mHomeServer = homeServerInformation;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class HomeServerInformation {
        @JsonProperty("base_url")
        private String baseUrl;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class IdentityServerInformation {
        @JsonProperty("base_url")
        private String baseUrl;
    }
}
