package org.mascarene.matrix.client.r0.model.auth.flow;

import lombok.Data;

import java.util.List;

@Data
public class Flow {
    private List<String> stages;
}
