package org.mascarene.matrix.client.r0.model.auth;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Data
public class AuthDataRequest {
    Map<String, String> auth;
}
