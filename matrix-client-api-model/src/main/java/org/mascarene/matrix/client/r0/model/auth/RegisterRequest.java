package org.mascarene.matrix.client.r0.model.auth;

import lombok.Data;

@Data
public class RegisterRequest extends AuthDataRequest {
    private String username;
    private String password;
    private String device_id;
    private String initial_device_display_name;
    private boolean inhibit_login;
}
