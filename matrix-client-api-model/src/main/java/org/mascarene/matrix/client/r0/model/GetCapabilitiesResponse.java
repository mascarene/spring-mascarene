package org.mascarene.matrix.client.r0.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCapabilitiesResponse {
    public Capabilities capabilities;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class Capabilities {
        @JsonProperty("m.change_password")
        public ChangePasswordCapability mChangePassword;
        @JsonProperty("m.room_versions")
        public RoomVersionsCapability mRoomVersions;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class ChangePasswordCapability {
        public boolean enabled;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static final class RoomVersionsCapability {
        @JsonProperty("default")
        private String _default;
        private Map<String, RoomVersionStability> available;

    }

    public enum RoomVersionStability {
        stable, unstable;
    }

}
