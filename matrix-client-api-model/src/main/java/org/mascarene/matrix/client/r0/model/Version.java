package org.mascarene.matrix.client.r0.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Version {
    private List<String> versions;
    private Map<String, Boolean> unstable_features;

    public Version(List<String> versions) {
        setVersions(versions);
        setUnstable_features(null);
    }
}