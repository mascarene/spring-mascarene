package org.mascarene.homeserver.tests;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import org.mascarene.homeserver.commons.MascareneProperties;
import org.mascarene.matrix.api.client.r0.server.ClientApiController;
import org.mascarene.matrix.client.r0.model.GetCapabilitiesResponse;
import org.mascarene.matrix.client.r0.model.Version;
import org.mascarene.matrix.client.r0.model.WellKnownResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@EnableConfigurationProperties(MascareneProperties.class)
@WebFluxTest(ClientApiController.class)
@ComponentScan("org.mascarene")
public class ClientApiTests {
    @Autowired
    WebTestClient webClient;

    @Test
    public void getVersionTest() {
        Version version = webClient.get().uri("/_matrix/client/versions").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(Version.class)
                .returnResult().getResponseBody();
        assertNotNull(version);
        assertFalse(version.getVersions().isEmpty());
    }

    @Test
    public void getWellKnown() {
        WellKnownResponse response = webClient.get().uri("/.well-known/matrix/client")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(WellKnownResponse.class)
                .returnResult().getResponseBody();
        assertNotNull(response);
    }

    @Test
    public void getCapabilities() {
        GetCapabilitiesResponse response = webClient.get().uri("/_matrix/client/r0/capabilities")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(GetCapabilitiesResponse.class)
                .returnResult().getResponseBody();
        assertNotNull(response);
    }
}
