package org.mascarene.homeserver.tests;

import org.junit.jupiter.api.Test;
import org.mascarene.homeserver.commons.MascareneProperties;
import org.mascarene.matrix.api.client.r0.server.AuthLoginController;
import org.mascarene.matrix.client.r0.model.auth.LoginAuthFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.junit.jupiter.api.Assertions.*;

@EnableConfigurationProperties(MascareneProperties.class)
@WebFluxTest(AuthLoginController.class)
@ComponentScan("org.mascarene")
public class LoginApiTests {
    @Autowired
    WebTestClient webClient;

    @Test
    void getLogin() {
        LoginAuthFlow loginAuthFlow = webClient.get().uri("/_matrix/client/r0/login").accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody(LoginAuthFlow.class)
                .returnResult().getResponseBody();
        assertNotNull(loginAuthFlow);
    }
}
