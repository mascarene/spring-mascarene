package org.mascarene.homeserver.tests;

import org.junit.jupiter.api.Test;
import org.mascarene.homeserver.commons.MascareneProperties;
import org.mascarene.matrix.api.client.r0.server.AuthLoginController;
import org.mascarene.matrix.client.r0.model.auth.AuthDataRequest;
import org.mascarene.matrix.client.r0.model.auth.flow.InteractiveAuthFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@EnableConfigurationProperties(MascareneProperties.class)
@WebFluxTest(AuthLoginController.class)
@ComponentScan("org.mascarene")
public class InteractiveAuthTests {
    @Autowired
    WebTestClient webClient;

    @Autowired
    MascareneProperties mascareneProperties;

    @Test
    void returnsNewInteractiveFlowWhenNoAuthData() {
        InteractiveAuthFlow authFlow = initInteractiveFlow("/_matrix/client/r0/register");
        assertNotNull(authFlow);
        assertNotNull(authFlow.getSession());
    }

    @Test
    void returnsErrorWithInvalidAuthType() {
        InteractiveAuthFlow initFlow = initInteractiveFlow("/_matrix/client/r0/register");
        Map<String, String> authData = new HashMap<>();
        authData.put("type", "m.test");
        authData.put("session", initFlow.getSession());
        AuthDataRequest request = new AuthDataRequest();
        request.setAuth(authData);
        Mono<AuthDataRequest> requestMono = Mono.just(request);
        InteractiveAuthFlow authFlow =
                webClient.post().uri("/_matrix/client/r0/register").body(requestMono, AuthDataRequest.class).accept(MediaType.APPLICATION_JSON)
                        .exchange()
                        .expectStatus().isUnauthorized()
                        .expectHeader().contentType(MediaType.APPLICATION_JSON)
                        .expectBody(InteractiveAuthFlow.class)
                        .returnResult().getResponseBody();
        assertNotNull(authFlow);
        assertNotNull(authFlow.getSession());
    }

    @Test
    void testInteractivity() {
        InteractiveAuthFlow initFlow = initInteractiveFlow("/_matrix/client/r0/register");
        Map<String, String> authData = new HashMap<>();
        authData.put("type", "m.login.password");
        authData.put("session", initFlow.getSession());
        AuthDataRequest request = new AuthDataRequest();
        request.setAuth(authData);
        Mono<AuthDataRequest> requestMono = Mono.just(request);
        InteractiveAuthFlow authFlow =
                webClient.post().uri("/_matrix/client/r0/register").body(requestMono, AuthDataRequest.class).accept(MediaType.APPLICATION_JSON)
                        .exchange()
                        .expectStatus().isUnauthorized()
                        .expectHeader().contentType(MediaType.APPLICATION_JSON)
                        .expectBody(InteractiveAuthFlow.class)
                        .returnResult().getResponseBody();
        assertNotNull(authFlow);
        assertNotNull(authFlow.getSession());
    }

    private InteractiveAuthFlow initInteractiveFlow(String endpoint) {
        return webClient.post().uri(endpoint).bodyValue("{}").accept(MediaType.APPLICATION_JSON)
                        .exchange()
                        .expectStatus().isUnauthorized()
                        .expectHeader().contentType(MediaType.APPLICATION_JSON)
                        .expectBody(InteractiveAuthFlow.class)
                        .returnResult().getResponseBody();
    }
}
