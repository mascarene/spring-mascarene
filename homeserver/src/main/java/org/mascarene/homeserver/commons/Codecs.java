package org.mascarene.homeserver.commons;

import org.springframework.util.Base64Utils;

import java.nio.ByteBuffer;
import java.util.UUID;

public class Codecs {
    public static String genSessionId() {
        return Base64Utils.encodeToUrlSafeString(getBytesFromUUID(UUID.randomUUID())).replace("=","");
    }

    public static byte[] getBytesFromUUID(UUID uuid) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());

        return bb.array();
    }

    public static UUID getUUIDFromBytes(byte[] bytes) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
        long high = byteBuffer.getLong();
        long low = byteBuffer.getLong();

        return new UUID(high, low);
    }
}
