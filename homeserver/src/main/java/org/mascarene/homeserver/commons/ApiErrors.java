package org.mascarene.homeserver.commons;

import org.mascarene.matrix.client.r0.model.ApiError;

public class ApiErrors {
    public static ApiError InternalError(String message){
        return new ApiError("ORG_MASCARENE_INTERNAL_ERROR", message);
    }

    public static ApiError NotJson(String message){
        return new ApiError("M_NOT_JSON", message);
    }

    public static ApiError BadJson(String message){
        return new ApiError("M_BAD_JSON", message);
    }
}
