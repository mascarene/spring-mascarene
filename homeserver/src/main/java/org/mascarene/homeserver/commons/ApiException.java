package org.mascarene.homeserver.commons;

import org.mascarene.matrix.client.r0.model.ApiError;

public class ApiException extends Exception {
    private final ApiError apiError;

    public ApiException(ApiError apiError) {
        super(String.format("%s: %s", apiError.getErrcode(), apiError.getError()));
        this.apiError = apiError;
    }

    public ApiException(ApiError apiError, Throwable cause) {
        super(String.format("%s: %s", apiError.getErrcode(), apiError.getError()));
        this.apiError = apiError;
    }

    public ApiError getApiError() {
        return apiError;
    }
}
