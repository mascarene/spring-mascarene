package org.mascarene.homeserver.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(MascareneProperties.class)
public class MascareneConfiguration {
    private MascareneProperties mascareneProperties;

    public MascareneConfiguration() {
    }

    public MascareneProperties getMascareneProperties() {
        return mascareneProperties;
    }

    @Autowired
    public void setMascareneProperties(MascareneProperties mascareneProperties) {
        this.mascareneProperties = mascareneProperties;
    }
}
