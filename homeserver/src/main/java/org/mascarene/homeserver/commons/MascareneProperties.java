package org.mascarene.homeserver.commons;

import lombok.Data;
import org.mascarene.matrix.client.r0.model.auth.flow.InteractiveAuthFlow;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;
import java.util.List;
import java.util.Map;

@ConfigurationProperties(prefix = "mascarene", ignoreUnknownFields = false)
@Data
public final class MascareneProperties {
    private MatrixProperties matrix;
    private ServerProperties server;

    @Data
    public static class MatrixProperties {
        private String defaultRoomVersion;
        private List<String> supportedRoomVersions;
        private CapabilitiesProperties capabilities;
        private Map<String, InteractiveAuthFlow> interactiveAuthFlows;
    }

    @Data
    public static class ServerProperties {
        private String baseUrl;
        private AuthSessionCacheProperties authSessionCache;
    }

    @Data
    public static class CapabilitiesProperties {
        private boolean changePassword;
    }

    @Data
    public static class AuthSessionCacheProperties {
        private Duration expireDelay;
        private long maximumSize;
    }
}
