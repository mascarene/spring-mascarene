package org.mascarene.homeserver.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mascarene.homeserver.commons.ApiErrors;
import org.mascarene.homeserver.commons.ApiException;
import org.mascarene.homeserver.commons.MascareneProperties;
import org.mascarene.matrix.client.r0.model.auth.AuthDataRequest;
import org.mascarene.matrix.client.r0.model.auth.flow.InteractiveAuthFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AuthService {
    private static final Log log = LogFactory.getLog(AuthService.class);
    private MascareneProperties mascareneProperties;
    private AuthSessionCache sessionCache;

    /**
     * Implements [Interactive auth flow](https://matrix.org/docs/spec/client_server/r0.6
     * .1#user-interactive-authentication-api)
     *
     * @param request
     * @return
     */
    public Mono<Optional<InteractiveAuthFlow>> checkAuthFlow(String endpoint, AuthDataRequest request) throws
            ApiException {
        if (request.getAuth() == null || request.getAuth().isEmpty())
            return createNewAuthFlow(endpoint);
        else {
            return checkCompleted(endpoint, request.getAuth());
        }
    }

    private Mono<Optional<InteractiveAuthFlow>> createNewAuthFlow(String endpoint) throws ApiException {
        try {
            // Initialize interfactive flow for the given endpoint from configuration,
            // store it to cachs and return the cached session with its Id
            InteractiveAuthFlow authFlow = mascareneProperties.getMatrix().getInteractiveAuthFlows().get(endpoint);
            return Mono.just(Optional.of(sessionCache.putSession(authFlow)));
        } catch (NullPointerException npe) {
            String message = "No interactive auth configuration found for endpoint " + endpoint;
            log.warn(message);
            throw new ApiException(ApiErrors.InternalError(message));
        }
    }

    private Mono<Optional<InteractiveAuthFlow>> checkCompleted(String endpoint, Map<String, String> authData)
            throws ApiException {
        // Check if request auth data contains a session Id
        // if not, return a new auth flow session
        final String interactiveSessionId = authData.get("session");
        if (interactiveSessionId == null)
            return createNewAuthFlow(endpoint);

        // Check if the given session Id is known from cache
        // if not, it may has expired, return a new auth flow session
        final Optional<InteractiveAuthFlow> interactiveSession = sessionCache.getSession(interactiveSessionId);
        if (interactiveSession.isEmpty())
            return createNewAuthFlow(endpoint).map(optFlow -> optFlow
                    .map(flow -> flow.withErrcode("M_FORBIDDEN").withError("Invalid / unknown session Id")));
        else {
            return challenge(interactiveSession.get(), authData);
        }
    }

    private Mono<Optional<InteractiveAuthFlow>> challenge(InteractiveAuthFlow authFlowSession,
                                                          Map<String, String> authData) {

        try {
            final String authType = validateAuthType(authData);
            final List<Optional<String>> nextStages = nextExpectedStages(authFlowSession);

            // Check if the given auth type is from one of the expected stages.
            final Optional<String> expectedAuthType =
            nextStages.stream().filter(optStr -> optStr.isPresent() && optStr.get().equals(authType)).map(Optional::get).findFirst();
            if (expectedAuthType.isEmpty())
                return Mono.just(Optional.of(
                        authFlowSession.withErrcode("M_FORBIDDEN").withError(
                                String.format("Unexpected auth type '%s'. Expecting one of {%s}", authType,
                                        String.join(",", nextStages.toString())))));

            // Challenge auth info
            InteractiveAuthFlow challengedAuthType = switch (authType) {
                case "m.login.dummy" -> authFlowSession.withCompleted(Stream.concat(authFlowSession.getCompleted().stream(),
                        Stream.of("m.login.dummy")).collect(Collectors.toList()));
                case "m.login.password" -> authFlowSession.withErrcode("IO.MASCARENE.NOT_IMPLEMENTED");
                case "m.login.recaptcha" -> authFlowSession.withErrcode("IO.MASCARENE.NOT_IMPLEMENTED");
                case "m.login.oauth2" -> authFlowSession.withErrcode("IO.MASCARENE.NOT_IMPLEMENTED");
                case "m.login.email.identity" -> authFlowSession.withErrcode("IO.MASCARENE.NOT_IMPLEMENTED");
                case "m.login.token" -> authFlowSession.withErrcode("IO.MASCARENE.NOT_IMPLEMENTED");
                default -> authFlowSession.withErrcode("M_FORBIDDEN").withError(
                        String.format("Unexpected auth type '%s'", authType));
            };
            sessionCache.updateSession(challengedAuthType);
            if(nextExpectedStages(challengedAuthType).isEmpty())
                return Mono.just(Optional.empty());
            else
                return Mono.just(Optional.of(challengedAuthType));
        }
        catch(ApiException e) {
            return Mono.just(Optional.of(authFlowSession.withErrcode(e.getApiError().getErrcode()).withError(e.getApiError().getError())));
        }
    }

    private final List<String> auth_types = List.of("m.login.password",
            "m.login.recaptcha",
            "m.login.oauth2",
            "m.login.sso",
            "m.login.email.identity",
            "m.login.msisdn",
            "m.login.token",
            "m.login.dummy");

    private String validateAuthType(Map<String, String> authData) throws ApiException {
        final String authType = authData.get("type");
        if ("".equals(authType))
            throw new ApiException(ApiErrors.BadJson("Auth type missing or empty"));
        if (!auth_types.contains(authType))
            throw new ApiException(ApiErrors.BadJson("Unknown auth type " + authType));
        return authType;
    }

    private List<Optional<String>> nextExpectedStages(InteractiveAuthFlow authSessionFlow) {
        final List<String> completedStages = authSessionFlow.getCompleted();
        return authSessionFlow.getFlows().stream().map(f -> {
            List<String> stages = new ArrayList<>(f.getStages());
            if(completedStages != null && !completedStages.isEmpty())
                stages.removeAll(completedStages);
            return Optional.ofNullable(stages.get(0));
        }).collect(Collectors.toList());
    }

    @Autowired
    public void setMascareneProperties(MascareneProperties mascareneProperties) {
        this.mascareneProperties = mascareneProperties;
    }

    @Autowired
    public void setSessionCache(AuthSessionCache sessionCache) {
        this.sessionCache = sessionCache;
    }
}
