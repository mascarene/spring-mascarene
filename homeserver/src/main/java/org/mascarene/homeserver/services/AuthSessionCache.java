package org.mascarene.homeserver.services;

import org.mascarene.homeserver.commons.Codecs;
import org.mascarene.matrix.client.r0.model.auth.flow.InteractiveAuthFlow;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public abstract class AuthSessionCache {
    protected abstract InteractiveAuthFlow internalGetSession(String sessionId);
    protected abstract InteractiveAuthFlow internalPutSession(InteractiveAuthFlow flow);
    protected abstract InteractiveAuthFlow internalUpdateSession(String sessionId, InteractiveAuthFlow flow);
    protected abstract void internalRemoveSession(String sessionId);

    protected String genSessionId() {
        return Codecs.genSessionId();
    }

    public Optional<InteractiveAuthFlow> getSession(String sessionId) {
        return Optional.ofNullable(internalGetSession(sessionId));
    }

    public InteractiveAuthFlow putSession(InteractiveAuthFlow flow) {
        return internalPutSession(flow);
    }

    public InteractiveAuthFlow updateSession(InteractiveAuthFlow flow) {
        return internalUpdateSession(flow.getSession(), flow);
    }

    public void removeSession(String sessionId) {
        internalRemoveSession(sessionId);
    }
}
