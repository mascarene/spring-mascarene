package org.mascarene.homeserver.services;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mascarene.homeserver.commons.MascareneProperties;
import org.mascarene.matrix.client.r0.model.auth.flow.InteractiveAuthFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("!cluster")
@Component
public class InMemoryAuthSessionCache extends AuthSessionCache {
    private static final Log log = LogFactory.getLog(InMemoryAuthSessionCache.class);
    private MascareneProperties mascareneProperties;

    private Cache<String, InteractiveAuthFlow> sessionCache;

    @Override
    protected InteractiveAuthFlow internalGetSession(String sessionId) {
        return sessionCache.getIfPresent(sessionId);
    }

    @Override
    protected InteractiveAuthFlow internalPutSession(InteractiveAuthFlow flow) {
        String sessionId = genSessionId();
        InteractiveAuthFlow flowCopy = flow.withSession(sessionId);
        sessionCache.put(sessionId, flowCopy);
        return flowCopy;
    }

    @Override
    protected InteractiveAuthFlow internalUpdateSession(String sessionId, InteractiveAuthFlow flow) {
        sessionCache.put(sessionId, flow);
        return flow;
    }

    @Override
    protected void internalRemoveSession(String sessionId) {
        sessionCache.invalidate(sessionId);
    }

    @Autowired
    public void setMascareneProperties(MascareneProperties mascareneProperties) {
        this.mascareneProperties = mascareneProperties;

        MascareneProperties.AuthSessionCacheProperties cacheProperties = mascareneProperties.getServer().getAuthSessionCache();
        if(sessionCache != null) {
            sessionCache.invalidateAll();
            sessionCache.cleanUp();
        }
        sessionCache =
                Caffeine.newBuilder().expireAfterWrite(cacheProperties.getExpireDelay()).maximumSize(cacheProperties.getMaximumSize()).build();
    }
}
