package org.mascarene.homeserver;

import com.hazelcast.client.config.ClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("cluster")
public class HazelCastConfiguration {
    @Bean
    public ClientConfig hazelCastClientConfig() {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.setInstanceName("mascarene");
        return clientConfig;
    }
}
