package org.mascarene.homeserver.db.repository;

import org.mascarene.homeserver.db.entities.Account;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import java.util.UUID;

public interface AccountRepository extends ReactiveCrudRepository<Account, UUID> {
}
