package org.mascarene.homeserver.db.repository;

import org.mascarene.homeserver.db.entities.Device;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import java.util.UUID;

public interface DeviceRepository extends ReactiveCrudRepository<Device, UUID> {
}
