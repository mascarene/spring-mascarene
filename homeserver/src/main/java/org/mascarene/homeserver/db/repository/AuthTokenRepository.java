package org.mascarene.homeserver.db.repository;

import org.mascarene.homeserver.db.entities.AuthToken;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import java.util.UUID;

public interface AuthTokenRepository extends ReactiveCrudRepository<AuthToken, UUID> {
}
