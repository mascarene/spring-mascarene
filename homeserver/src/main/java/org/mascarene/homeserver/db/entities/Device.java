package org.mascarene.homeserver.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;
import java.util.UUID;

@Table("devices")
@Data
@AllArgsConstructor
public class Device {
    @Id
    private UUID deviceId;
    private String mxDeviceId;
    private String displayName;
    private UUID accountId;
    private Instant lastSeen;
    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant lastModifiedAt;
}
