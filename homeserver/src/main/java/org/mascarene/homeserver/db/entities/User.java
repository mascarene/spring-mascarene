package org.mascarene.homeserver.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Table("users")
@Data
@AllArgsConstructor
public class User {
    @Id
    private UUID userId;
    private String mxUserId;
    private String displayName;
    private String avatarUrl;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant lastModifiedAt;

    public User(UUID userId, String mxUserId, String displayName, String avatarUrl) {
        this.userId = userId;
        this.mxUserId = mxUserId;
        this.displayName = displayName;
        this.avatarUrl = avatarUrl;
    }

    public Optional<String> getDisplayName() {
        return Optional.ofNullable(displayName);
    }

    public Optional<String> getAvatarUrl() {
        return Optional.ofNullable(avatarUrl);
    }

    public Optional<Instant> getLastModifiedAt() {
        return Optional.ofNullable(lastModifiedAt);
    }
}
