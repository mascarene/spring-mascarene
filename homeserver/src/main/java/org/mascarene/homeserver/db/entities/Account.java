package org.mascarene.homeserver.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;
import java.util.UUID;

@Table("accounts")
@Data
@AllArgsConstructor
public class Account {
    @Id
    private UUID accountId;
    private UUID userId;
    private String passwordHash;
    private String kind;
    private boolean deactivated;
    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant lastModifiedAt;
}
