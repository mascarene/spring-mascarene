package org.mascarene.homeserver.db.repository;

import org.mascarene.homeserver.db.entities.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import java.util.UUID;

public interface UserRepository extends ReactiveCrudRepository<User, UUID> {
}
