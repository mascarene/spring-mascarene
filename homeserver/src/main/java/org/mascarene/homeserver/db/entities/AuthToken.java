package org.mascarene.homeserver.db.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;
import java.util.UUID;

@Table("auth_tokens")
@Data
@AllArgsConstructor
public class AuthToken {
    @Id
    private UUID tokenId;
    private UUID accountId;
    private UUID deviceId;
    private String encodedToken;
    private Instant lastUsed;
    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant lastModifiedAt;
}
