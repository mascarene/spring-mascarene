package org.mascarene.homeserver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mascarene.homeserver.commons.ApiErrors;
import org.mascarene.homeserver.commons.ApiException;
import org.mascarene.homeserver.services.AuthService;
import org.mascarene.matrix.client.r0.model.ApiError;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Component
@Order(-2)
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {
    private static final Log log = LogFactory.getLog(GlobalExceptionHandler.class);
    private final Jackson2JsonEncoder encoder = new Jackson2JsonEncoder();

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        log.debug("GlobalExceptionHandler caught exception", ex);
        //Init default response
        ApiError apiError = ApiErrors.InternalError(ex.getMessage());
        HttpStatus responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        //Customize response according to exception type and details
        if(ex instanceof ApiException) {
            apiError = ((ApiException) ex).getApiError();
        }
        if(ex instanceof ResponseStatusException) {
            responseStatus = ((ResponseStatusException) ex).getStatus();
            if(HttpStatus.NOT_FOUND.equals(((ResponseStatusException) ex).getStatus()))
                apiError.setErrcode("M_NOT_FOUND");
        }

        //Encode and return response
        Flux<DataBuffer> flux = encoder.encode(Mono.just(apiError), new DefaultDataBufferFactory(),
                ResolvableType.forClass(ApiError.class),
                MediaType.APPLICATION_JSON,
                Collections.emptyMap());
        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
        exchange.getResponse().setStatusCode(responseStatus);
        return exchange.getResponse().writeWith(flux);
    }
}
