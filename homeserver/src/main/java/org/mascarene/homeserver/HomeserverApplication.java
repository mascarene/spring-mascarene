package org.mascarene.homeserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "org.mascarene")
public class HomeserverApplication {
    public static void main(String[] args) {
        SpringApplication.run(HomeserverApplication.class, args);
    }

}
