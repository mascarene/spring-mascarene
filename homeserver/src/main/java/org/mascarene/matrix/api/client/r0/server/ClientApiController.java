package org.mascarene.matrix.api.client.r0.server;

import org.mascarene.homeserver.commons.MascareneProperties;
import org.mascarene.matrix.client.r0.model.*;
import org.mascarene.matrix.client.r0.model.GetCapabilitiesResponse.*;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ClientApiController {

    private MascareneProperties mascareneProperties;

    @Autowired
    public void setMascareneProperties(MascareneProperties mascareneProperties) {
        this.mascareneProperties = mascareneProperties;
    }

    @GetMapping("/_matrix/client/versions")
    public Publisher<ResponseEntity<Version>> getVersion() {
        return Mono.just(ResponseEntity.ok(new Version(List.of("0.6.0"))));
    }

    @GetMapping("/_matrix/client/r0/capabilities")
    public Publisher<ResponseEntity<GetCapabilitiesResponse>> getCapabilities() {
        final MascareneProperties.MatrixProperties matrixProperties = mascareneProperties.getMatrix();
        final MascareneProperties.CapabilitiesProperties capabilitiesProperties = matrixProperties.getCapabilities();

        RoomVersionsCapability roomVersionsCapability = new RoomVersionsCapability(
                matrixProperties.getDefaultRoomVersion(), matrixProperties.getSupportedRoomVersions().stream()
                .collect(Collectors.toMap(v -> v, v -> RoomVersionStability.stable)));
        final Capabilities capabilities = new Capabilities(
                new ChangePasswordCapability(capabilitiesProperties.isChangePassword()), roomVersionsCapability);
        return Mono.just(ResponseEntity.ok(new GetCapabilitiesResponse(capabilities)));
    }

    @GetMapping("/.well-known/matrix/client")
    public Publisher<ResponseEntity<WellKnownResponse>> getWellKnown() {
        final MascareneProperties.ServerProperties serverProperties = mascareneProperties.getServer();
        final WellKnownResponse response =
                new WellKnownResponse(new WellKnownResponse.HomeServerInformation(serverProperties.getBaseUrl()));
        return Mono.just(ResponseEntity.ok(response));
    }
}
