package org.mascarene.matrix.api.client.r0.server;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mascarene.homeserver.commons.ApiErrors;
import org.mascarene.homeserver.commons.ApiException;
import org.mascarene.homeserver.services.AuthService;
import org.mascarene.matrix.client.r0.model.auth.AuthDataRequest;
import org.mascarene.matrix.client.r0.model.auth.flow.InteractiveAuthFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.DecodingException;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.http.server.PathContainer;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;

import static org.springframework.core.ResolvableType.forClass;

@Component
public class WithAuthFlow implements WebFilter {
    private AuthService authService;
    private final Jackson2JsonDecoder decoder = new Jackson2JsonDecoder();
    private final Jackson2JsonEncoder encoder = new Jackson2JsonEncoder();
    ResolvableType authType = forClass(AuthDataRequest.class);

    private static final Log log = LogFactory.getLog(WithAuthFlow.class);
    private static final List<Pair<String, HttpMethod>> authInteractiveEndpoints =
            List.of(Pair.of("/_matrix/client/r0/login", HttpMethod.POST),
                    Pair.of("/_matrix/client/r0/register", HttpMethod.POST));

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
        String requestUriPath = exchange.getRequest().getURI().getPath();
        HttpMethod requestMethod = exchange.getRequest().getMethod();
        if (!authInteractiveEndpoints.contains(Pair.of(requestUriPath, requestMethod)))
            return chain.filter(exchange);
        else {
            //The API endpoint call requires interactive auth
            log.debug(String.format("Filtering %s %s call with interactive auth flow, chaining filters",
                    requestMethod.name(), requestUriPath));
            List<String> pathElems = new ArrayList<>();

            //use last part of URI as key for auth configuration lookup
            for (PathContainer.Element e : exchange.getRequest().getPath().pathWithinApplication().elements()) {
                if (!(e instanceof PathContainer.Separator)) {
                    pathElems.add(e.value());
                }
            }
            if (pathElems.isEmpty()) {
                return chain.filter(exchange);
            } else {
                String endpoint = pathElems.get(pathElems.size() - 1);
                log.debug("interactive API end point: " + endpoint);

                return decoder.decodeToMono(exchange.getRequest().getBody().single(),
                        authType,
                        MediaType.APPLICATION_JSON,
                        Collections.emptyMap())
                        .map(m -> {
                            log.debug("test "+ m);
                            return m;
                        })
                        .cast(AuthDataRequest.class)
                        .flatMap(request -> {
                            try {
                                return authService.checkAuthFlow(endpoint, request);
                            }
                            catch (ApiException e) {
                                return Mono.error(e);
                            }
                }).flatMap( r -> {
                    if(r.isEmpty()) {
                        //CheckAuthFlow is empty, all stages are completed
                        //Auth flow doesn't need any more stages to complete, continue request process
                        return chain.filter(exchange);
                    }
                    else {
                        //One or more stages need to be checked
                        //return interactives response
                        Flux<DataBuffer> flux = encoder.encode(Mono.just(r.get()), new DefaultDataBufferFactory(),
                                ResolvableType.forClass(InteractiveAuthFlow.class),
                                MediaType.APPLICATION_JSON,
                                Collections.emptyMap());
                        exchange.getResponse().getHeaders().setContentType(MediaType.APPLICATION_JSON);
                        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
                        return exchange.getResponse().writeWith(flux);
                    }
                        })
                        .onErrorMap(NoSuchElementException.class, t -> new ApiException(ApiErrors.BadJson(t.getMessage())))
                        .onErrorMap(DecodingException.class, t -> new ApiException(ApiErrors.NotJson(t.getMessage())))
                        .onErrorMap(ClassCastException.class, t -> new ApiException(ApiErrors.BadJson(t.getMessage())))
                        ;
            }
        }
    }

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }
}
