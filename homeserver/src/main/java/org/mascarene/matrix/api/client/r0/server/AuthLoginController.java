package org.mascarene.matrix.api.client.r0.server;

import org.mascarene.homeserver.commons.MascareneProperties;
import org.mascarene.matrix.client.r0.model.auth.LoginAuthFlow;
import org.mascarene.matrix.client.r0.model.auth.flow.Flow;
import org.mascarene.matrix.client.r0.model.auth.flow.InteractiveAuthFlow;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.*;

@RestController
@RequestMapping("/_matrix/client/r0")
public class AuthLoginController {
    private MascareneProperties mascareneProperties;

    @Autowired
    public void setMascareneProperties(MascareneProperties mascareneProperties) {
        this.mascareneProperties = mascareneProperties;
    }

    @Autowired
    public AuthLoginController(MascareneProperties mascareneProperties) {
        this.mascareneProperties = mascareneProperties;
    }

    @GetMapping("/login")
    public Publisher<ResponseEntity<LoginAuthFlow>> getLogin() {
        List<LoginAuthFlow.LoginFlow> supportedFlows = new ArrayList<>();
        Set<String> uniqueValues = new HashSet<>();
        for (InteractiveAuthFlow f : mascareneProperties.getMatrix().getInteractiveAuthFlows().values()) {
            for (Flow flow : f.getFlows()) {
                for (String s : flow.getStages()) {
                    if (uniqueValues.add(s)) {
                        supportedFlows.add(new LoginAuthFlow.LoginFlow(s));
                    }
                }
            }
        }
        return Mono.just(ResponseEntity.ok(new LoginAuthFlow(supportedFlows)));
    }
}
