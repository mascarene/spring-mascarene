/*
 * Mascarene
 * Copyright (C) 2020 mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

CREATE TABLE server_keys
(
    server_key_id UUID PRIMARY KEY,
    server_name   VARCHAR                  NOT NULL,
    key_id        VARCHAR                  NOT NULL,
    public_key    BYTEA,
    expired_at    TIMESTAMP WITH TIME ZONE,
    valid_until   TIMESTAMP WITH TIME ZONE,
    created_at    TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE
);
CREATE
UNIQUE INDEX idx_server_keys_unique ON server_keys(server_name, key_id);