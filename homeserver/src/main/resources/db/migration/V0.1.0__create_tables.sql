CREATE TABLE users
(
    user_id         UUID PRIMARY KEY,
    mx_user_id      VARCHAR NOT NULL UNIQUE,
    display_name    VARCHAR,
    avatar_url      VARCHAR,
    created_at      TIMESTAMP,
    lastmodified_at TIMESTAMP
);

CREATE TABLE accounts
(
    account_id    UUID PRIMARY KEY,
    user_id       UUID REFERENCES users (user_id) ON DELETE CASCADE,
    password_hash VARCHAR,
    kind          VARCHAR CHECK (kind in ('user', 'guest')),
    created_at    TIMESTAMP WITH TIME ZONE,
    updated_at    TIMESTAMP WITH TIME ZONE
);

CREATE TABLE devices
(
    device_id    UUID PRIMARY KEY,
    mx_device_id VARCHAR                  NOT NULL,
    display_name VARCHAR,
    account_id   UUID REFERENCES accounts (account_id) ON DELETE CASCADE,
    last_seen    TIMESTAMP WITH TIME ZONE,
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at   TIMESTAMP WITH TIME ZONE
);

CREATE TABLE auth_tokens
(
    token_id      UUID PRIMARY KEY,
    account_id    UUID REFERENCES accounts (account_id) ON DELETE CASCADE,
    device_id     UUID REFERENCES devices (device_id) ON DELETE CASCADE,
    encoded_token VARCHAR,
    last_used     TIMESTAMP WITH TIME ZONE,
    created_at    TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE
);

CREATE TABLE account_data
(
    account_id    UUID REFERENCES accounts (account_id) ON DELETE CASCADE,
    event_type    VARCHAR                  NOT NULL,
    event_content JSONB,
    created_at    TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE
);
CREATE
UNIQUE INDEX idx_account_data_unique ON account_data(account_id, event_type);

CREATE TABLE room_account_data
(
    account_id    UUID REFERENCES accounts (account_id) ON DELETE CASCADE,
    room_id       UUID,
    event_type    VARCHAR                  NOT NULL,
    event_content JSONB,
    created_at    TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at    TIMESTAMP WITH TIME ZONE
);
CREATE
UNIQUE INDEX idx_room_account_data_unique ON room_account_data(account_id, room_id, event_type);

CREATE TABLE filters
(
    id                UUID PRIMARY KEY,
    account_id        UUID REFERENCES accounts (account_id) ON DELETE CASCADE,
    filter_definition JSONB,
    created_at        TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at        TIMESTAMP WITH TIME ZONE
);

CREATE TABLE rooms
(
    id                    UUID PRIMARY KEY,
    mx_room_id            VARCHAR                  NOT NULL,
    visibility            VARCHAR                  NOT NULL,
    version               VARCHAR                  NOT NULL,
    last_stateset_version BIGINT,
    created_at            TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at            TIMESTAMP WITH TIME ZONE
);

CREATE TABLE events
(
    id               UUID PRIMARY KEY,
    mx_event_id      VARCHAR,
    state_key        VARCHAR,
    sender_id        UUID REFERENCES users (user_id),
    room_id          UUID REFERENCES rooms (id),
    event_type       VARCHAR                  NOT NULL,
    origin_server_ts TIMESTAMP WITH TIME ZONE,
    unsigned         JSONB,
    rejected         BOOLEAN,
    resolved         BOOLEAN,
    processed        BOOLEAN,
    stateset_version BIGINT,
    stream_order     BIGINT,
    received_ts      TIMESTAMP WITH TIME ZONE,
    rejection_cause  VARCHAR,
    created_at       TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE event_auth_edges
(
    event_id         UUID REFERENCES events (id),
    auth_event_mx_id VARCHAR,
    auth_event_id    UUID REFERENCES events (id)
);

CREATE TABLE event_parent_edges
(
    event_id           UUID REFERENCES events (id),
    parent_event_mx_id VARCHAR,
    parent_event_id    UUID REFERENCES events (id)
);

CREATE TABLE event_contents
(
    event_id UUID REFERENCES events (id) ON DELETE CASCADE,
    content  JSONB
);

CREATE TABLE stateset_versions
(
    room_id        UUID REFERENCES rooms (id) ON DELETE CASCADE,
    version        BIGINT NOT NULL,
    state_event_id UUID REFERENCES events (id)
);

CREATE TABLE distributed_data
(
    key   VARCHAR NOT NULL PRIMARY KEY,
    value JSONB
);

CREATE TABLE room_memberships
(
    room_id    UUID REFERENCES rooms (id) ON DELETE CASCADE,
    user_id    UUID REFERENCES users (user_id),
    event_id   UUID references events (id),
    membership VARCHAR                  NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE
);
CREATE
UNIQUE INDEX idx_room_memberships_unique on room_memberships(room_id, user_id);

CREATE TABLE api_transactions
(
    transaction_id   UUID PRIMARY KEY,
    path             VARCHAR                  NOT NULL,
    txn_id           VARCHAR                  NOT NULL UNIQUE,
    auth_token_id    UUID REFERENCES auth_tokens (token_id) ON DELETE CASCADE,
    response_content JSONB,
    created_at       TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE room_aliases
(
    alias_id   UUID PRIMARY KEY,
    room_id    UUID REFERENCES rooms (id) ON DELETE CASCADE,
    creator_id UUID REFERENCES users (user_id),
    alias      VARCHAR UNIQUE           NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE room_aliases_servers
(
    alias_id      UUID REFERENCES room_aliases (alias_id) ON DELETE CASCADE,
    server_domain VARCHAR                  NOT NULL,
    created_at    TIMESTAMP WITH TIME ZONE NOT NULL
)