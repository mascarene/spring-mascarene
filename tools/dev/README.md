## Mascarene configuration for development

This samples provides prerequisites for running mascarene in developer environment. It provides:
 - a PostgreSQL database
 - a Redis database
 - a Nginx reverse proxy for exposing Riot and mascarene through HTTPS

### Configuration

#### Database

Development requires two PostgreSQL database.
 1. one for storing Mascarene data
 2. one for storing Synapse test instance

Before runnning Mascarene and Synapse, you need to create the appropriate PostgreSQL database:

```

$ docker-compose exec db psql -U postgres postgres
postgres=# CREATE USER mascarene WITH ENCRYPTED PASSWORD 'some_password';
postgres=# CREATE DATABASE mascarene OWNER mascarene;
postgres=# CREATE USER synapse WITH ENCRYPTED PASSWORD 'synapse';
postgres=# CREATE DATABASE synapse ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER synapse;
```

#### Mascarene

See `mascarene.conf`

### Running

```
docker-compose up -d
```

Then run `org.mascarene.homeserver.MascareneMain` class with VM argument `-Dconfig.file=./docker/dev/mascarene.conf` 

